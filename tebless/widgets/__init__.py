"""Contain all widgets for screen.

"""

from ._window import Window
from ._menu import Menu
from ._input import Input
from ._filter_menu import FilterMenu
from ._label import Label
