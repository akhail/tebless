# Copyright (c) 2017 Michel Betancourt
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT
"""Global constants

"""


import blessed

TERM = blessed.Terminal()
ENTER = TERM.KEY_ENTER
ESC = TERM.KEY_ESCAPE
DOWN = TERM.KEY_DOWN
UP = TERM.KEY_UP
BACKSPACE = TERM.KEY_BACKSPACE
DEL = TERM.KEY_DELETE
